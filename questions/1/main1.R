# Importa o dataset
library(AppliedPredictiveModeling)
data(solubility)

# RMSE
library(caret)

#----- Funções -----#
# Separa os dados de treino
training_data = solTrainXtrans

# Adicionar os dados de solução de treino ao dataset
training_data$Solubility = solTrainY

# Faz um modelo linear utilizando todos os predictors
lm_all_pred = lm(Solubility~., data = training_data)

# Cria um novo conjunto de dados a partir dos predictors dados de teste e do
# modelo criado
lm_pred_1 = predict(lm_all_pred, solTestXtrans)

# Armazena o resultados preditos com os reais em um data frame
lm_val_1 = data.frame(obs = solTestY, pred = lm_pred_1)

par(mfrow=c(1,2))
scatter.smooth(lm_val_1)

# Resíduos
res = solTestY - lm_pred_1

plot(lm_pred_1, res, xlab = "Pred", ylab = "Resid")
abline(0,0)

summ = defaultSummary(lm_val_1)
print(summ[1])
print(summ[2])
# RMSE entre os valores preditos e os valores reais
#RMSE(lm_val_1, solTrainY)
#r2 = rsq(as.vector(lm_val_1), as.vector(solTrainY)) 

ctrl = trainControl(method = "cv", number = 10)

set.seed(100)
lm_fit_1 = train(x = solTrainXtrans, y = solTrainY, method = "lm", trControl = ctrl)

